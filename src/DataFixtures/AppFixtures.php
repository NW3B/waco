<?php

namespace App\DataFixtures;

use App\Entity\Article;
use App\Entity\Categorie;
use App\Entity\User;
use DateTimeImmutable;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

class AppFixtures extends Fixture
{
    private $passwordHasher;

    public function __construct(UserPasswordHasherInterface $passwordHasher)
    {
        $this->passwordHasher = $passwordHasher;
    }


    public function load(ObjectManager $manager): void
    {
        // Utilisation de Faker
        //require_once 'vendor/autoload.php';
        $faker = Factory::create('fr_FR');
        // Création d'un utilisateur
        $user = new User();
        $user->setEmail('user@test.com')
             ->setPrenom($faker->firstName())
             ->setNom($faker->lastName())
             ->setRoles(['ROLE_USER']);

        $password = $this->passwordHasher->hashPassword($user, 'password');
        $user->setPassword($password);

        $manager->persist($user);


        // Création de 5 Catégories
        for ($k = 0; $k < 5; $k++) {
            $categorie = new Categorie();

            $categorie->setNom($faker->word())
                      ->setDescription($faker->words(10, true))
                      ->setSlug($faker->slug());

            $manager->persist($categorie);
            // Création de 10 Article/catégorie
            for ($j = 0; $j < 10; $j++) {
                $article = new Article();

                //Création DateTimeImmutable
                $date = $faker->dateTimeBetween('-6 month', 'now');
                $immutable = DateTimeImmutable::createFromMutable($date);

                $article->setNom($faker->words(3, true))
                        ->setReference($faker->randomNumber())
                        ->setDescription($faker->text())
                        ->setOffre($faker->text())
                        ->setSlug($faker->slug())
                        ->setFile('net.jpg')
                        ->addCategorie($categorie)
                        ->setUser($user)
                        ->setPrix($faker->randomFloat(2, 100, 9999))
                        ->setPdf('fiche.pdf')
                        ->setCreatedAt($immutable);

                $manager->persist($article);
            }
        }

        $manager->flush();
    }
}
