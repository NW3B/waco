<?php

namespace App\Entity;

class PropertySearch
{
    /**
     * @var string|null
     */
    private $mots;


    /**
     * Get the value of mots
     *
     * @return  string|null
     */
    public function getMots()
    {
        return $this->mots;
    }

    /**
     * Set the value of mots
     *
     * @param  string|null  $mots
     *
     * @return  self
     */
    public function setMots($mots)
    {
        $this->mots = $mots;

        return $this;
    }
}
