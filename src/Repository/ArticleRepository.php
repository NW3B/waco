<?php

namespace App\Repository;

use App\Entity\Article;
use App\Entity\Categorie;
use App\Entity\PropertySearch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Article|null find($id, $lockMode = null, $lockVersion = null)
 * @method Article|null findOneBy(array $criteria, array $orderBy = null)
 * @method Article[]    findAll()
 * @method Article[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ArticleRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Article::class);
    }

    /**
    * @return Article[] Returns an array of Article objects
    */

    public function articleByRef()
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.createdAt', 'DESC')
            ->getQuery()
            ->getResult()
        ;
    }

    /**
    * @return Article[] Returns an array of Peinture objects
    */
    public function findAllArticle(Categorie $categorie): array
    {
        return $this->createQueryBuilder('a')
            ->where(':categorie MEMBER OF a.categorie')
            ->setParameter('categorie', $categorie)
            ->getQuery()
            ->getResult()
        ;
    }

    /**
    * @return
    */

    public function artquery()
    {
        return $this->createQueryBuilder('a')
            ->orderBy('a.reference', 'ASC')
            ->setMaxResults(100)
        ;
    }

    /**
    * @return
    */
    public function catQuery(Categorie $categorie)
    {
        return $this->createQueryBuilder('a')
            ->where(':categorie MEMBER OF a.categorie')
            ->setParameter('categorie', $categorie)
        ;
    }

    /**
    * Recherche les articles en fonction du formulaire depuis la vu home
    * @return void
    */
    public function search(PropertySearch $search, Categorie $categorie = null)
    {
        $query = $this->artquery();

        if ($search->getMots()) {
            $query = $this->createQueryBuilder('a')
                          ->where('MATCH_AGAINST(a.nom, a.description) AGAINST
                                (:mots boolean)>0')
                          ->setParameter('mots', $search->getMots());
        }
        if (isset($categorie)) {
            $query = $this->createQueryBuilder('a')
                          ->where(':categorie MEMBER OF a.categorie')
                          ->setParameter('categorie', $categorie);
        }

        return $query->getQuery();
    }

    /**
    * Recherche les articles en fonction du formulaire depuis la vu categorie
    * @return void
    */
    public function searchCat(PropertySearch $search, Categorie $categorie)
    {
        $query = $this->catQuery($categorie);

        if ($search->getMots()) {
            $query = $this->createQueryBuilder('a')
                          ->where('MATCH_AGAINST(a.nom, a.description) AGAINST
                                (:mots boolean)>0')
                          ->setParameter('mots', $search->getMots());
        }

        return $query->getQuery();
    }


    // /**
    //  * @return Article[] Returns an array of Article objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Article
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
