<?php

namespace App\Controller\Admin;

use App\Entity\Article;
use EasyCorp\Bundle\EasyAdminBundle\Config\Crud;
use EasyCorp\Bundle\EasyAdminBundle\Controller\AbstractCrudController;
use EasyCorp\Bundle\EasyAdminBundle\Field\AssociationField;
use EasyCorp\Bundle\EasyAdminBundle\Field\BooleanField;
use EasyCorp\Bundle\EasyAdminBundle\Field\DateTimeField;
use EasyCorp\Bundle\EasyAdminBundle\Field\ImageField;
use EasyCorp\Bundle\EasyAdminBundle\Field\NumberField;
use EasyCorp\Bundle\EasyAdminBundle\Field\SlugField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextareaField;
use EasyCorp\Bundle\EasyAdminBundle\Field\TextField;
use Vich\UploaderBundle\Form\Type\VichImageType;
use Vich\UploaderBundle\Form\Type\VichFileType;

class ArticleCrudController extends AbstractCrudController
{
    public static function getEntityFqcn(): string
    {
        return Article::class;
    }

    public function configureFields(string $pageName): iterable
    {
        return [

            TextField::new('reference')->hideOnIndex(),
            TextField::new('nom'),
            AssociationField::new('categorie')->hideOnIndex(),
            TextField::new('imageFile')->setFormType(VichImageType::class)->hideOnIndex()
                                       ->setFormTypeOptions([
                                        'allow_delete' => false]),
            TextareaField::new('description'),
            TextareaField::new('offre'),
            SlugField::new('slug')->setTargetFieldName('nom')->hideOnIndex(),
            //NumberField::new('prix'),
            //MoneyField::new('prix')->setCurrency('EUR'),
            ImageField::new('file')->setBasePath('/uploads/articles/')->onlyOnIndex(),
            //ImageField::new('pdf')->setBasePath('/uploads/pdf/')->onlyOnIndex(),
            //DateTimeField::new('createdAt'),
            TextField::new('pdfFile')->setFormType(VichFileType::class)->hideOnIndex(),
        ];
    }

    public function configureCrud(Crud $crud): Crud
    {
        return $crud
            ->setDefaultSort(['createdAt' => 'DESC'])
            ->setPageTitle('index', 'Catalogue')
            ->setPageTitle('edit', 'Modifier un Article');
    }
}
