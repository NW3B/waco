<?php

namespace App\Controller;

use App\Entity\Article;
use App\Entity\Categorie;
use App\Entity\PropertySearch;
use App\Form\PropertySearchType;
use App\Repository\ArticleRepository;
use App\Repository\UserRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index(
        ArticleRepository $articleRepository,
        UserRepository $userRepository,
        PaginatorInterface $paginator,
        Request $request
    ): Response {
        $search = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);

        $query = $articleRepository->search($search);
        $articles = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            12
        );

        return $this->render('home/index.html.twig', [
            'articles' => $articles,
            'form' => $form->createView(),
            'admin' => $userRepository->getAdmin(),
        ]);
    }


    /**
     * @Route("/cat_{slug}", name="article_categorie")
     */
    public function categorie(
        Categorie $categorie,
        ArticleRepository $articleRepository,
        PaginatorInterface $paginator,
        UserRepository $userRepository,
        Request $request
    ) {
        $search = new PropertySearch();
        $form = $this->createForm(PropertySearchType::class, $search);
        $form->handleRequest($request);

        $query = $articleRepository->searchCat($search, $categorie);
        $articles = $paginator->paginate(
            $query,
            $request->query->getInt('page', 1),
            12
        );

        if ($form->isSubmitted() && $form->isValid()) {
        }

        return $this->render('home/index.html.twig', [
            'categorie' => $categorie,
            'articles' => $articles,
            'form' => $form->createView(),
            'admin' => $userRepository->getAdmin(),
        ]);
    }


    /**
     * @Route("/detail/{slug}", name="article_details")
     */
    public function details(Article $article, UserRepository $userRepository): Response
    {
        return $this->render('home/details.html.twig', [
            'article' => $article,
            'admin' => $userRepository->getAdmin(),
        ]);
    }
}
