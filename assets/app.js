/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.scss';

import { Tooltip, Toats, Popover } from 'bootstrap';

// start the Stimulus application
import './bootstrap';
import './js/vendor/popper.js';
//import './js/vendor/bootstrap.min.js';
import './js/jquery.ajaxchimp.min.js';
import './js/jquery.nice-select.min.js';
import './js/jquery.sticky.js';
import './js/nouislider.min.js';
import './js/owl.carousel.min.js';
import './js/main.js';
//import './js/gmaps.min.js';

import './js/countdown.js';
import './js/parallax.min.js';
import './js/ion.rangeSlider.js';

import './js/vendor/jquery-2.2.4.min.js';
import './js/jquery.magnific-popup.min.js';

