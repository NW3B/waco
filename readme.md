# Waco

* Waco est un site internet présentant un catalogue de produit. Back Office Inclus.

* Hébergement temporaire -> https://wacocatalogue.osc-fr1.scalingo.io/   ( SITE DOWN ) 
* Back office -> https://wacocatalogue.osc-fr1.scalingo.io/admin   user : user@test.com  mdp : password   

## Environnement de développement

### Pré-requis

* PHP 7.4
* Composer
* Symfony CLI
* Docker
* Docker-compose
* nodejs et npm

Vous pouvez vérifier les pré-requis (sauf Docker et Docker-compose) avec la commande suivante (de la CLI Symfony) :

```bash
symfony check:requirements
```

### Lancer l'environnement de développement

```bash
composer install
npm install
npm run build
docker-compose up -d
symfony serve -d
```

### Migrations

```bash
symfony console doctrine:migrations:migrate
```

### Ajouter des données de test

```bash
symfony console doctrine:fixtures:load
```

### Lancer des tests

```bash
php bin/phpunit --testdox
```
