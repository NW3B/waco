<?php

namespace App\Tests;

use App\Entity\Categorie;
use App\Entity\Article;
use App\Entity\User;
use PHPUnit\Framework\TestCase;

class ArticleUnitTest extends TestCase
{
    public function testIsTrue()
    {
        $article = new Article();
        $categorie = new Categorie();
        $user = new User();

        $article->setNom('nom')
                 ->setDescription('description')
                 ->setSlug('slug')
                 ->setFile('file')
                 ->addCategorie($categorie)
                 ->setReference('ref')
                 ->setOffre('offre');
         
        $this->assertTrue($article->getNom() === 'nom');
        $this->assertTrue($article->getDescription() === 'description');
        $this->assertTrue($article->getSlug() === 'slug');
        $this->assertTrue($article->getFile() === 'file');
        $this->assertContains($categorie, $article->getCategorie());
        $this->assertTrue($article->getReference() === 'ref');
        $this->assertTrue($article->getOffre() === 'offre');

    }

    public function testIsFalse()
    {
        $article = new Article();
        $categorie = new Categorie();
        $user = new User();

        $article->setNom('nom')
                 ->setDescription('description')
                 ->setSlug('slug')
                 ->setFile('file')
                 ->addCategorie($categorie)
                 ->setReference('ref')
                 ->setOffre('offre');

        $this->assertFalse($article->getNom() === 'false');
        $this->assertFalse($article->getDescription() === 'false');
        $this->assertFalse($article->getSlug() === 'false');
        $this->assertFalse($article->getFile() === 'false');
        $this->assertNotContains(new Categorie(), $article->getCategorie());
        $this->assertFalse($article->getReference() === 'false');
        $this->assertFalse($article->getOffre() === 'false');
    }

    public function testIsEmpty()
    {
        $article = new Article();

        $this->assertEmpty($article->getNom());
        $this->assertEmpty($article->getDescription());
        $this->assertEmpty($article->getSlug());
        $this->assertEmpty($article->getFile());
        $this->assertEmpty($article->getCategorie());
        $this->assertEmpty($article->getId());
        $this->assertEmpty($article->getReference());
        $this->assertEmpty($article->getOffre());
    }

}
